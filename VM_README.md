## Installation

* Install VirtualBox
* Install Oracle VM VirtualBox Extension Pack
* Download the image from http://apparat.ee/tmp/fp2.ova
* Install the VM image to VirtualBox as usual

## Running the FacePhysio demo

User: `m`, pass: `fp`

```
cd backend
git pull
chmod +x start_server.sh # this seems to fall back often with git pushes
sudo ./start_server.sh
```

* FireFox homepage should be the URL of the demo: `localhost/home`
* I set up NAT port forwarding from the VM (get IP with `ifconfig`, port 80) and host machine (IP 0.0.0.0, choose arbitrary port). This way the JS rendering works faster, I think because the refresh rate is poor in the VM.
* You can share the `backend` folder on your host machine with the VM (under VM settings in VirtualBox > Shared Folders). This will be automatically mounted as /media/sf_backend. You can run `./link_to_host.sh` on the VM to symlink some of the relevant files to the ones in your host machine, so that you can edit on your host machine with your preferred editor, but compile/run it on the VM. Before running `git pull`, you should run `./unlink_from_host.sh` first.
* When you `start_server.sh` and then shut it down with `Ctrl+C` and try to start the server again, there is a websockets exception. This goes away in a few minutes or after VM restart, then you can `start_server.sh` again.
