var svg = d3.select("svg")

function x1(d, i) { return x_margin + i*(x_width + x_margin); }
function x2(d, i) { return x1(d, i) + x_width; }
function cx(d, i) { return (x1(d, i) + x2(d, i)) / 2; }
function cy(d, i) {
    var cy_new = y(d, i) - bell_margin;
    if (cy_new < cy_prev[i]) {
        cy_updated(d_prev[i], d);
        cy_prev[i] = cy_new;
        return cy_new;
    } else {
        return cy_prev[i];
    }
}

function cy_updated(d_old, d_new) {
    if (state == 1) {
        var new_prog = progress + (d_new - d_old) / 2;
        progress = Math.min(new_prog, 1);
    }
}
function progress_bar_x2(d, i) {
    var x2 = progress_bar_margin + progress*progress_bar_width;
    return(x2);
}

function cy_delay(d, i) {
    var cy_new = y(d, i) - bell_margin;
    var cy_prev_tmp = y(d_prev[i], i) - bell_margin;

    var bell_raises = cy_new < cy_prev_tmp;
    var brow_is_below = y_prev[i] <= cy_prev_tmp + bell_margin;
    var not_first = y_prev[i] != y_base;
    if (i == 0) {
    }

    if (bell_raises && brow_is_below && not_first) {
        // var full_distance = y((d - d_prev[i]), i);
        var full_distance = cy_new - y_prev[i];
        var y_prev_to_cy_prev = cy_prev_tmp - y_prev[i] + 20;
        var fraction_of_distance = y_prev_to_cy_prev / full_distance;
        var time_to_reach_bell = transition_time * fraction_of_distance;

        // console.log("full distance = cy_new("+ cy_new +") - y_prev[i]("+ y_prev[i] +") = " + full_distance);
        // console.log("y_prev_to_cy_prev = y_prev[i]("+ y_prev[i] +") - cy_prev_tmp("+ cy_prev_tmp +") = " + y_prev_to_cy_prev);
        // console.log("fraction_of_distance = full_distance("+ full_distance +") / y_prev_to_cy_prev("+ y_prev_to_cy_prev +") = " + fraction_of_distance);
        // console.log("time_to_reach_bell "+ time_to_reach_bell);

        // return(time_to_reach_bell);
        return(0);
    } else {
        return(0);
    }
}
function y(d, i) { return y_base - d*y_range; }
function update_y(d, i) {
    var ret = y(d, i);
    y_prev[i] = ret;
    d_prev2[i] = d_prev[i];
    d_prev[i] = d;
    if (i==0){
        console.log("  moving y to " + y(d,i));
    }
    return ret;
}

function update(data) {
    update_bells(data);
    update_brows(data);
    update_progress_bar();
}

function update_progress_bar(data) {
    var progress_bar = svg.selectAll("line.progress_bar")
        .data([progress], function(d, i) { return i; });

    progress_bar.enter().append("line")
    .attr("class", "progress_bar")
    .attr("y1", 80)
    .attr("y2", 80)
    .attr("x1", 100)
    .attr("x2", progress_bar_x2)
    .attr("stroke", progress_bar_color);

    updteSel = progress_bar.transition()
                   .duration(transition_time)
                   .attr("x2", progress_bar_x2)
                   .attr("stroke", progress_bar_color);

    progress_bar.exit().remove();
}

function brow_color(d,i) {
    d_curr[i] = d;
    var diff_left_right = Math.abs(d_curr[0] - d_curr[1]);
    var bar_increases = (d_prev2[i] - d) < 0;
    if (i == 1) {
        if (diff_left_right > state_threshold) {
            state = -1;
        } else if (diff_left_right == 0) {
            state = 0;
        } else {
            if (bar_increases) {
                state = 1;
            }  else {
                state = 1;
            }
        }
    }
    switch(state) {
        case -1:
            return "#3c53d2";
            break;
        case 1:
            return "#07946d";
            break;
        case 0: // default
            return "#ccc";
            break;
    }
};

function update_brows(data) {
    var brow = svg.selectAll("line.brow")
        .data(data, function(d, i) { return i; });

    brow.enter().append("line")
    .attr("class", "brow")
    .attr("y1", y)
    .attr("y2", y)
    .attr("x1", x1)
    .attr("x2", x2)
    .attr("stroke", brow_color);

    updteSel = brow.transition()
                   .duration(transition_time)
                //    .ease(d3.easeLinear)
                   .attr("y1", update_y)
                   .attr("y2", update_y)
                   .attr("stroke", brow_color);

    brow.exit().remove();
}

function update_bells(data) {
    var bell = svg.selectAll("circle.bell")
        .data(data, function(d, i) { return i; });
    bell.enter().append("circle")
        .attr("class", "bell")
        .attr("cx", cx)
        .attr("cy", cy)
        .attr("r", 10);

    updteSel = bell.transition()
                 .duration(transition_time)
                //  .ease(d3.easeLinear)
                //  .delay(cy_delay)
                 .attr("cx", cx)
                 .attr("cy", cy)
                 .attr();

    bell.exit().remove();
}


function run_json(json) {
    i = 0;
    var interval_new_data = setInterval(function() {
        new_data();
    }, transition_time);


    function new_data() {
        if (i >= json.length) {
            clearInterval(interval_new_data);
        } else {
            update(json[i]);
        }
        i++;
    }
}


var width = 700;
var y_base = 400;
var y_range = 200;
var x_margin = 100;
var x_width = 200;
var bell_margin = 20;
var d_prev = [0, 0];
var d_prev2 = [0, 0];
var d_curr = [0,0];
var y_prev = [y_base, y_base];
var cy_prev = [y_base - bell_margin, y_base - bell_margin];

var state = 0; // -1, 0, 1
var state_threshold = 0.1;
var progress = 0; // [0, 1]
var bump_brow = 0.1;
var bump_state = 0.05;
var progress_bar_margin = 100;
var progress_bar_width = width - 2*progress_bar_margin;
var progress_bar_color = d3.scaleLinear().domain([0,1]).range(["#3c53d2", "#07946d"]);

var slowdown = 1;
var transition_time = 41*slowdown;

var ws = new silicon_json_websocket('ws://' + window.location.host);

function random_points() {
  return([Math.random(), Math.random()]);
}

ws.api.message = function (m) { 
  console.log(m.text);
  update(random_points());
}

// document.getElementById('form').onsubmit = function (e) {
//     if (input.value.length > 0) ws.broadcast({message: input.value });
//     input.value = "";
//     return false;
// };