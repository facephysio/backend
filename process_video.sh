#!/bin/bash
# Copyright (C) 2017 FacePhysio

if [ $# -ne 1 ]
  then
    echo "Usage: process_video.sh <filename>"
    exit 1
fi

FILE="$1"

echo "Processing ${FILE} file..."
./OpenFace/build/bin/FeatureExtraction -rigid  -verbose -f "${FILE}" -of "output/features.txt"
echo "Results stored in \"output\" directory."

