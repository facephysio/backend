#!/bin/bash

# Exit script if any command fails
set -e 
set -o pipefail

if [ $# -ne 1 ]
  then
    echo "Usage: install.sh <directory in which you want the project to be installed>"
    exit 1
fi

DIRECTORY="$1"
cd "$DIRECTORY"
echo "Installation under ${DIRECTORY}"

# Essential Dependencies
echo "Installing Essential dependencies..."
sudo apt-get -y update
sudo apt-get -y install build-essential
sudo apt-get -y install llvm

sudo apt-get -y update
sudo apt-get -y install clang-3.7 libc++-dev libc++abi-dev
sudo apt-get -y install cmake
sudo apt-get -y install libopenblas-dev liblapack-dev
sudo apt-get -y install git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get -y install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev checkinstall
echo "Essential dependencies installed."

# OpenCV Dependency
echo "Downloading OpenCV..."
if [ -d "opencv-3.1.0" ]; then
  sudo rm -r "opencv-3.1.0"
fi

wget https://github.com/Itseez/opencv/archive/3.1.0.zip
unzip 3.1.0.zip
rm 3.1.0.zip
cd opencv-3.1.0
mkdir build
cd build
echo "Installing OpenCV..."
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_SHARED_LIBS=OFF ..
make -j4
sudo make install
cd "../.."
echo "OpenCV installed."

# Boost C++ Dependency
echo "Installing Boost..."
sudo apt-get -y install libboost-all-dev
echo "Boost installed."

# Websocketspp Dependency
if [ ! -d /usr/local/include/websocketpp ]; then
  echo "Installing Websocketpp..."
  wget https://github.com/zaphoyd/websocketpp/archive/master.zip
  unzip master.zip
  sudo mv websocketpp-master/websocketpp /usr/local/include
  sudo rm -r websocketpp-master
  rm master.zip
  echo "Websocketpp installed."
fi

# Silicon C++ Dependency
if [ ! -d /usr/local/include/silicon ]; then
  echo "Installing Silicon..."
  sudo apt-get -y install dconf-tools
  git clone https://github.com/matt-42/silicon.git
  cd silicon
  sudo ./install.sh /usr/local
  cd ..
  echo "Silicon Installed."
fi

echo "Installing OpenFace..."
cd OpenFace
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE ..
make -j4
echo "OpenFace installed."


# Installation test
echo "Testing installation..."
./bin/FaceLandmarkVid -f "../videos/changeLighting.wmv"
echo "Installation tested."