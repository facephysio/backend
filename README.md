# OpenFace Integration with FacePhysio
*Author:* Daniyal Shahrokhian

## Installation

### Ubuntu 16.04 LTE
1. Run `sudo gedit /etc/apt/sources.list` and change the line `deb http://us.archive.ubuntu.com/ubuntu/ xenial main restricted` to `deb http://us.archive.ubuntu.com/ubuntu/ xenial main universe`

2. Run `install.sh <directory in which you want the project to be installed>`

### Mac
Follow the steps in [OpenFace's Wiki page](https://github.com/TadasBaltrusaitis/OpenFace/wiki/Mac-installation)

## Usage

### Demo video
Run `OpenFace/build/bin/FaceLandmarkVid [-f <filename>] [-ov <output filename>]` (without arguments it starts recording the webcam)

### Extract different features into folder
Run `OpenFace/process_video.sh <filename>`

### Live Webcam FeatureExtraction
Run `OpenFace/build/bin/FeatureExtraction -of <output file>`

### Stream webcam features to a frontend
Run `start_server.sh`


`output.txt` contains 2D landmark points, gaze and action units. 

For more details regarding output format, check https://github.com/TadasBaltrusaitis/OpenFace/wiki/Output-Format#featureextraction

### Integration notes
All changes have been made to the files `FeatureExtraction.cpp` and `MessagingServer.cpp`. In the original repository, this code doesn't have a live-demo from the webcam, neither the messaging server. I added both features in such file. 

When calculating the average eyebrow coordinates, refer to the commit titles and this [wiki page](https://github.com/TadasBaltrusaitis/OpenFace/wiki/Output-Format) in particular, to understand the conversion done in `MessagingServer`.

Take a look [here](https://github.com/matt-42/silicon/blob/master/examples/ws_broadcast_server.cc#L33-L40) to modify the frontend for receiving push notifications from the backend. In summary, I used websockets, which has an implementation in javascript (checkout these [templates](https://github.com/matt-42/silicon/tree/master/client_templates)). All the client has to do is to connect to the server port (8080 by default) and the server will start streaming data. Multiple clients can connect to this server.

Take into account that the landmark outputs of OpenFace are in Matlab standard: the origin of the image is (1,1). I didn't add such subtraction because I was told that the landmarks would later be normalized [0,1].

## Links
Repository: https://github.com/TadasBaltrusaitis/OpenFace

Wiki: https://github.com/TadasBaltrusaitis/OpenFace/wiki

Streaming server: https://github.com/matt-42/silicon