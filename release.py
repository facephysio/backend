#!/usr/bin/python

import sys
import os.path
import os 
from vtop import Cpic, Cvid, Cphoto
import subprocess
from PIL import Image, ImageDraw

def dr_point(path, newpath, xy):
    im = Image.open(path)
    for i in range(0,67):
        draw = ImageDraw.Draw(im)
        draw = draw.ellipse((int(xy[i*2])/2-3, int(xy[i*2+1])/2-3, int(xy[i*2])/2+3, int(xy[i*2+1])/2+3), fill = "red", outline = "red")
    im.save(newpath, "JPEG")

sys.path.append(os.getcwd())

path_to_initial_video = sys.argv[1] 
path_to_initial_images = sys.argv[2]
path_to_post_images = sys.argv[3]
path_to_post_video = sys.argv[4]

pic = Cpic(folder=path_to_initial_images)
pic.create(path = path_to_initial_video)

os.chdir(path_to_initial_images)  
str_ = " ".join(os.listdir())

path_to_store_points = "../points.txt"
f = open(path_to_store_points, "w")

subprocess.call("../face2.exe ../shape_predictor_68_face_landmarks.dat "+str_, stdout = f)
os.chdir("../")

points_file = open("points.txt") 
points = points_file.read()
points_list = points.splitlines()

for i in range(0, len(points_list)):
    point_target = points_list[i].split(",")
    im_target = os.getcwd() + "/" + path_to_initial_images + "/" + (str)(i+1) + ".jpg"
    dr_point(im_target, os.getcwd() + "/" + path_to_post_images + "/" + (str)(i+1) + ".jpg", point_target) 


#subprocess.call(" ".join(["Rscript", "alter.R", path_to_initial_images, path_to_post_images, "points.txt", "1280", "720"]))

##eye_b_level = subprocess.check_output("Rscript ../pipeline.R "+temp_path, universal_newlines = True)
##print(eye_b_level)

vid = Cvid(path=os.getcwd() + "/" + path_to_post_images)
vid.create(
        newfilename=os.getcwd() + "/" + path_to_post_video,
        name="%1d.jpg")