# Dockerizing OpenFace: Dockerfile for building OpenFace images

FROM ubuntu:xenial

# Command Dependencies
RUN apt-get update && apt-get install -y \
	wget \
	unzip \
	make

# Essential Dependencies
RUN apt-get install -y \
	build-essential \
	llvm \
	clang-3.7 \
	libc++-dev \
	libc++abi-dev \
	cmake \
	libopenblas-dev \
	liblapack-dev \
	git \
	libgtk2.0-dev \
	pkg-config \
	libavcodec-dev \
	libavformat-dev \
	libswscale-dev \
	python-dev \
	python-numpy \
	libtbb2 \
	libtbb-dev \
	libjpeg-dev \
	libpng-dev \
	libtiff-dev \
	libjasper-dev \
	libdc1394-22-dev \
	wget \
	checkinstall

# OpenCV Dependency
RUN wget https://github.com/Itseez/opencv/archive/3.1.0.zip && \
	unzip 3.1.0.zip && \
	rm 3.1.0.zip && \
	cd opencv-3.1.0 && \
	mkdir build && \
	cd build && \
	cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_SHARED_LIBS=OFF .. && \
	echo "MADEEEEEE" && \
	make -j4 && \
	echo "MADEEEEEE 2222" && \
	make install && \
	echo "MADEEEEEE 3333" && \
	cd "../.."

# Boost C++ Dependency
RUN apt-get install -y libboost-all-dev

# Silicon C++ Dependency
RUN git clone https://github.com/matt-42/silicon.git && \
	cd silicon && \
	./install.sh /usr/local && \
	cd ".."

# OpenFace Dependency
ADD OpenFace /OpenFace
RUN cd OpenFace && \
	mkdir build && \
	cd build && \
	cmake -D CMAKE_BUILD_TYPE=RELEASE .. && \
	make

EXPOSE 8080
