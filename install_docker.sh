docker build -t openface .

docker run -p 8080:8080 \
	-e DISPLAY=$DISPLAY \
	--env="QT_X11_NO_MITSHM=1" \
	--privileged -v /dev/video0:/dev/video0 \
	--privileged -v /tmp/.X11-unix:/tmp/.X11-unix:ro  \
	openface